function addStudent() {
    let table = document.getElementById("table");
    let row = table.insertRow(table.length);
    let cell1 = row.insertCell(0);
    cell1.classList.add("col");
    let cell2 = row.insertCell(1);
    cell2.classList.add("col");
    let cell3 = row.insertCell(2);
    cell3.classList.add("col");
    let cell4 = row.insertCell(3);
    cell4.classList.add("col");
    let cell5 = row.insertCell(4);
    cell5.classList.add("col");
    let cell6 = row.insertCell(5);
    cell6.classList.add("col");
    let cell7 = row.insertCell(6);
    cell7.classList.add("col");
    let span = document.createElement('span');
    let checkbox = document.createElement('input');
    span.appendChild(checkbox);
    checkbox.type = 'checkbox';
    checkbox.addEventListener("click", () => {changeCircleColor(cell6)});
    cell1.appendChild(checkbox);
    cell2.innerHTML = "<span>PZ-21</span>"
    cell3.innerHTML = "<span>Maksym Potopliak</span>"
    cell4.innerHTML = "<span>M</span>"
    cell5.innerHTML = "<span>02.03.2004</span>"
    cell6.innerHTML = '<span class="circle d-inline-block"></span>';
    let spanContainer = document.createElement("span");
    spanContainer.classList.add("buttonContainer", "d-flex", "justify-content-center");
    cell7.appendChild(spanContainer);
    let buttonEdit = document.createElement("button");
    buttonEdit.classList.add("tableButton", "p-0");
    spanContainer.appendChild(buttonEdit);
    let penIcon = document.createElement("i");
    penIcon.classList.add("fa-solid", "fa-pen");
    buttonEdit.appendChild(penIcon);
    let removeButton = document.createElement("button");
    removeButton.classList.add("tableButton", "p-0");
    spanContainer.appendChild(removeButton);
    let removeIcon = document.createElement("i");
    removeIcon.classList.add("fa-solid", "fa-xmark");
    removeButton.appendChild(removeIcon);
    removeButton.addEventListener("click", () => {removeStudent(row)});
}
function removeStudent(el) {
    if(confirm("Are you sure you want to delete user?"))
        el.remove();
}
function changeCircleColor(tableCell){
    let spanCircle = tableCell.querySelectorAll('.circle').item(0);
    if(spanCircle.classList.contains('active')){
        spanCircle.classList.remove('active')
    }else{
        spanCircle.classList.add('active')
    }
}

function initListeners(){
    document.getElementById("buttonAddStudent").addEventListener("click", addStudent)
}